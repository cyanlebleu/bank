import { TokensService } from './../../services/tokens.service';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { LoginService } from '../../services/login.service';
import swal from 'sweetalert2';
import { AuthService } from 'src/services/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginFormGroup: FormGroup;
  alertController: AlertController;
  localStorage: Storage;

  constructor(private loginService: LoginService,
              private router: Router,
              private authService: AuthService,
              public loadingController: LoadingController,
              private formBuilder: FormBuilder,
              public tokens: TokensService) {
  }

  ngOnInit() {
    this.loginFormGroup = this.formBuilder.group({
      holder: '',
      account: '',
      password: '',
      rememberMe: false
    });
    const lembrar = localStorage.getItem('rememberMe') || null;
    if (lembrar != null) {
      this.loginFormGroup.setValue({
        holder: localStorage.getItem('holder'),
        account: localStorage.getItem('account'),
        password: localStorage.getItem('password'),
        rememberMe: true
      });
    }
  }

  mask(input: string) {
    return input.replace('/^(0|[1-9][0-9]*)$/', '');
  }

  async login(form) {
    await this.loading();
    if (form.value.rememberMe) {
      localStorage.setItem('account', form.value.account);
      localStorage.setItem('password', form.value.password);
      localStorage.setItem('holder', form.value.holder);
      localStorage.setItem('rememberMe', form.value.rememberMe);
    } else {
      localStorage.removeItem('account');
      localStorage.removeItem('password');
      localStorage.removeItem('holder');
      localStorage.removeItem('rememberMe');
    }
    this.loginService.login(form.value.holder, form.value.account, form.value.password).subscribe(resp => {
        this.authService.login();
        this.tokens.accessToken = resp.headers.get('access-token');
        this.tokens.client = resp.headers.get('client');
        this.tokens.uid = resp.headers.get('uid');
        this.tokens.holder = resp.body.data.holder;
        this.tokens.account = resp.body.data.account;
        this.loadingController.dismiss();
        this.router.navigate(['/dashboard', {}]);
    }, err => {
      this.loadingController.dismiss();
      this.authService.login();
      console.log(err);
      swal.fire('Acesso negado', err.error.errors[0]);
    });
  }

  async loading() {
    const loading = await this.loadingController.create({
      message: 'Entering app...',
    });
    await loading.present();
  }
}
