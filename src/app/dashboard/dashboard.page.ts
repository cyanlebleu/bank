import { Component } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';
import { BalanceModalComponent } from '../balance-modal/balance-modal.component';
import { AuthService } from 'src/services/auth.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.page.html',
  styleUrls: ['dashboard.page.scss'],
})
export class DashboardPage {

  constructor(private modalController: ModalController,
              private authService: AuthService,
              private router: Router,
              private loadingController: LoadingController) {}

  async showBalance() {
    const modal = await this.modalController.create({
      component: BalanceModalComponent,
      componentProps: {
        value: 123,
        height: '50%',
        width: '100%',
      },
      animated: true
    });

    return await modal.present();
  }

  logout() {
    swal.fire({
      title: 'Quit app',
      text: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, leave app'}).then(async (result) => {
        if (result.value) {
          await this.loading();
          this.authService.logout();
          this.router.navigate(['/login', {}]);
          this.loadingController.dismiss();
        }
      });
  }

  async loading() {
    const loading = await this.loadingController.create({
      message: 'Leaving app...',
    });
    await loading.present();
  }

}
