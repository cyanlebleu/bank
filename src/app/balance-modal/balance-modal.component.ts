import { Component, OnInit } from '@angular/core';
import { BalanceService } from 'src/services/balance.service';
import { ModalController } from '@ionic/angular';
import { TokensService } from 'src/services/tokens.service';

@Component({
  selector: 'app-balance-modal',
  templateUrl: './balance-modal.component.html',
  styleUrls: ['./balance-modal.component.scss'],
})
export class BalanceModalComponent implements OnInit {
  available_limit: any;
  balance: any;
  blocked_amount: any;
  limit: any;
  error: any;

  constructor(private balanceService: BalanceService,
              private modalController: ModalController,
              private tokens: TokensService) { }

  ngOnInit() {
    this.balanceService.balance(this.tokens.holder, this.tokens.account).subscribe(resp => {
            this.available_limit = resp['available_limit'];
            this.balance = resp['balance'];
            this.blocked_amount = resp['blocked_amount'];
            this.limit = resp['limit'];
          },
          err => {
            console.log(err);
        });
  }

  closeModal(){
    this.modalController.dismiss();
  }

}
