import { BalanceModalComponent } from './balance-modal.component';
import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';

@NgModule({
    declarations: [
        BalanceModalComponent
    ],
    imports: [
        IonicModule,
        CommonModule
    ],
    entryComponents: [
        BalanceModalComponent
    ]
})
export class BalanceModalComponentModule {}
