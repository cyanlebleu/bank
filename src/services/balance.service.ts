import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokensService } from './tokens.service';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {
  loadingController: any;

  constructor(private http: HttpClient,
              public tokens: TokensService) { }

  public balance(holder: string, account: string) {
    const url = 'https://smo.banking.homologa.bititecnologia.com.br/api/v2/user/balance';
    const uid = this.tokens.uid;
    const client = this.tokens.client;
    const accessToken = this.tokens.accessToken;
    const httpOptions = {
      headers: new HttpHeaders({
        'uid': uid,
        'client': client,
        'access-token': accessToken,
      })
    };

    return this.http.get(url, httpOptions);
  }
}
