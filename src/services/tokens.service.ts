import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokensService {

  public accessToken: string;
  public uid: string;
  public client: string;
  public holder: string;
  public account: string;

  constructor() { }
}
