import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaderResponse } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(holder: string, account: string, password: string): Observable<any> {
    const body = { account , password, holder };
    const url = 'https://smo.banking.homologa.bititecnologia.com.br/api/v2/auth/sign_in';
    return this.http.post(url, body, {headers: {'Content-Type': 'application/json'}, observe: 'response'});
  }
}
